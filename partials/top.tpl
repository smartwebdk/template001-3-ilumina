{includeScript file='partials/body.js.tpl'}

{*** Languages collection ***}
{collection controller=language assign=languages}

{*** Get all shop currencies ***}
{collection controller=currency assign=currencies}

{*** Generate site menu ***}
{$static = false}
{if $template.settings.SETTINGS_SHOW_MY_ACCOUNT}
    {$static = true}
{/if}
{menu assign=primaryMenu static=$static}

{*** Calculate if header bar is going to be populated ***}
{$hasEmptyHeader = $languages->getActualSize() lte 1 and $currencies->getActualSize() lte 1 and empty($primaryMenu)}

<div class="site-corporate">
    {if !$hasEmptyHeader}
    <div class="container with-xlarge">
        <div class="row">
            <div class="col-s-4 col-m-12 col-l-4 col-xl-8">

                {if $languages->getActualSize() gt 1 || $currencies and $currencies->getActualSize() gt 1}

                    <div class="skip-content">

                        {if $languages->getActualSize() gt 1}
                            <div class="w-language-picker is-inline-block">
                                {$showFlag = true}
                                {$showText = true}
                                {if $template.settings.SETTINGS_TYPE_LANGUAGE eq 'FLAG'}
                                    {$showText = false}
                                {elseif $template.settings.SETTINGS_TYPE_LANGUAGE eq 'TEXT'}
                                    {$showFlag = false}
                                {/if}

                                {include file="modules/widgets/language/language.tpl" collection=$languages type="dropdown" showFlag=$showFlag showText=$showText}
                            </div>
                        {/if}

                        {if $currencies and $currencies->getActualSize() gt 1}

                            <div class="w-currency-picker is-inline-block">
                                {include file="modules/widgets/currency/currency.tpl" collection=$currencies type="dropdown"}
                            </div>
                        {/if}

                    </div>

                {/if}

            </div>
            <div class="col-s-4 col-m-12 col-l-8 col-xl-16 is-hidden-s is-hidden-m">

                <div class="skip-content pull-right">

                    {include
                        file='modules/widgets/menu/menu.tpl'
                        items=$primaryMenu
                        classes='nav nav-default'
                    }

                </div>
            </div>
        </div>
    </div>
    {/if}
</div>
<header class="site-header{if $hasEmptyHeader} site-header--is-top{/if}">
    <div class="container with-xlarge">
        <div class="row">
            <div class="col-s-4 col-m-12 col-l-3 col-xl-6">
                <div class="brand">
                    {include file='modules/widgets/logo/logo.tpl'}
                </div>

                <ul class="skip-links is-hidden-l is-hidden-xl is-hidden-print">
                    <li>
                        {if !empty($primaryMenu)}
                        <button class="nav-toggle" title="{$text.WHERE_AM_I_NAVIGATION}" data-toggle="collapse" data-target="nav-menu">
                            <i class="fa fa-bars fa-fw"></i>
                        </button>
                        {/if}
                        {if $template.settings.SETTINGS_SHOW_SEARCH}
                        <button class="nav-toggle" title="{$text.SEARCH}" data-toggle="collapse" data-target="nav-search">
                            <i class="fa fa-search fa-fw"></i>
                        </button>
                        {/if}
                        {if $general.isShop and $template.settings.SETTINGS_SHOW_CART and $page.type != 'cart' and $page.type != 'checkout'}
                            {collection controller=cart assign=cart}

                            {$cartList = $cart->groupByClass()}

                            <a href="/{$text.CART_LINK}/" title="{$text.CART_MENU}" class="nav-toggle nav-toggle-cart {if isset($cartList.CartProductLine)}cart--active{/if}">
                                <span style="overflow: hidden; height: 1px; width: 0px; font-size: 0px; text-indent: -9999px;">&nbsp;</span>
                                <i class="fa fa-shopping-cart fa-fw">&nbsp;</i>
                            </a>
                        {/if}
                    </li>
                </ul>
            </div>
            <div class="col-s-4 col-m-12 col-l-9 col-xl-18 is-hidden-print">

                <div class="header--interaction row">

                    <div class="col-s-4 col-m-12 col-l-6 col-xl-12">
                        {if $template.settings.SETTINGS_SHOW_SEARCH}
                            <div class="skip-content w-search is-collapsed" data-group="nav-search">

                                <form method="get" action="/{if $general.isShop}{page id=$page.productPageId print=Link}{else}{$Text.SEARCH_LINK}{/if}/">
                                    <div class="input-group">
                                        <input type="text" class="form-input input-group-main" placeholder="{$Text.SEARCH_TEXT}" name="search" required>
                                        <span class="input-group-button"><button class="button" type="submit"><i class="fa fa-fw fa-search"></i></button></span>
                                    </div>
                                </form>

                            </div>
                        {/if}
                    </div>

                    <div class="col-s-4 col-m-12 col-l-6 col-xl-12 is-hidden-s is-hidden-m">

                        {if $general.isShop and $template.settings.SETTINGS_SHOW_CART and $page.type != 'cart' and $page.type != 'checkout'}
                            {include file='modules/widgets/cart/cart.tpl' cart=$cart}
                        {/if}
                    </div>

                    {if !empty($primaryMenu)}
                    <div class="col-s-4 col-m-12 col-l-12 col-xl-12 is-hidden-l is-hidden-xl">
                        <div class="skip-content is-collapsed" data-group="nav-menu">

                            {include
                                file='modules/widgets/menu/menu.tpl'
                                items=$primaryMenu
                                classes='nav nav-default'
                                subclasses='nav nav-menu'
                            }
                            <hr>

                        </div>
                    </div>
                    {/if}
                </div>

            </div>
        </div>

    </div>
</header>
{if $general.isShop and $page.type != 'cart' and $page.type != 'checkout'}
<div class="site-categories">
    <div class="container with-xlarge site-wrapper">
        <div class="row">
            <div class="col-s-4 col-m-12 col-l-12 col-xl-24">

                {* Fetch product categories *}
                {controller type=productCategory assign=controller primary=true}
                {collection controller=productCategory assign=categories}

                {* Fetch products link *}
                {$productLink = {page id=$page.productPageId print=Link}}

                <div class="panel panel-category">
                    <div class="panel-heading is-hidden-l is-hidden-xl is-hidden-print">
                        <div class="row">
                            <div class="col-s-3 col-m-8 col-l-8 col-xl-16">
                                <span class="h5">{$text.CATEGORYS}</span>
                            </div>
                            <div class="col-s-1 col-m-4 col-l-4 col-xl-8">
                                <button type="submit" class="button xsmall pull-right panel-category-button is-closed" title="{$text.SITEMAP_VIEW_ALL_CATEGORIES}" data-toggle="collapse" data-class="is-collapsed" data-target="panel-categories">
                                    <i class="fa fa-angle-double-up icon-close"></i>
                                    <i class="fa fa-angle-double-down icon-open"></i>
                                </button>
                            </div>
                        </div>
                    </div>
                    <div class="panel-body is-collapsed" data-group="panel-categories">

                        {if $categories->getActualSize() gt 0}
                            <ul class="category--list">
                                {foreach $categories->getData() as $category}

                                    <li class="category--item {if $category->IsActive}active{/if}">
                                        <a href="/{$productLink}/{$category->Handle}/">
                                            <div class="category--title">
                                                <span>{$category->Title}</span>
                                            </div>
                                            <div class="category--icon">
                                                <i class="fa fa-fw fa-long-arrow-right"></i>
                                            </div>
                                        </a>


                                        {if $catLevels gt 0 and ($foldout or $category->IsActive)}
                                            {* Fetch product categories level 1 *}
                                            {collection controller=productCategory assign=categories1 parentId=$category->Id}
                                            {if $categories1->getActualSize() gt 0}
                                                <ul class="level1">
                                                    {foreach $categories1->getData() as $category1}
                                                        <li class="{if $category1->IsActive}active{/if}">
                                                            <a href="/{$productLink}/{$category1->Handle}/">{$category1->Title}</a>

                                                            {*
                                                            {if $catLevels gt 1 and ($foldout or $category1->IsActive)}
                                                                {collection controller=productCategory assign=categories2 parentId=$category1->Id}
                                                                {if $categories2->getActualSize() gt 0}
                                                                    <ul class="level2">
                                                                        {foreach $categories2->getData() as $category2}
                                                                            <li class="{if $category2->IsActive}active{/if}">
                                                                                <a href="/{$productLink}/{$category2->Handle}/">{$category2->Title}</a>
                                                                                {if $catLevels gt 2 and ($foldout or $category2->IsActive)}
                                                                                    {collection controller=productCategory assign=categories3 parentId=$category2->Id}
                                                                                    {if $categories3->getActualSize() gt 0}
                                                                                        <ul class="level3">
                                                                                            {foreach $categories3->getData() as $category3}
                                                                                                <li class="{if $category3->IsActive}active{/if}">
                                                                                                    <a href="/{$productLink}/{$category3->Handle}/">{$category3->Title}</a>
                                                                                                </li>
                                                                                            {/foreach}
                                                                                        </ul>
                                                                                    {/if}
                                                                                {/if}
                                                                            </li>
                                                                        {/foreach}
                                                                    </ul>
                                                                {/if}
                                                            {/if}
                                                            *}
                                                        </li>
                                                    {/foreach}
                                                </ul>
                                            {/if}
                                        {/if}
                                    </li>
                                {/foreach}
                            </ul>
                        {/if}
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
{/if}
