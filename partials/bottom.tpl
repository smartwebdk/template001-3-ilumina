<footer class="site-footer">
	<div class="container with-xlarge">
		<div class="row">

			<div class="col-s-4 col-m-6 col-l-3 col-xl-6">
				<div class="footer-sitemap">
					{if $general.isShop}
						<p class="h4">{$text.CATEGORYS}</p>

						{* Fetch product categories *}
						{collection controller=productCategory assign=categories}

						{* Fetch products link *}
						{$productLink = {page id=$page.productPageId print=Link}}

						{if $categories->getActualSize() gt 0}
							<ul class="list-unstyled">
								{foreach $categories->getData() as $category}
									<li class="{if $category->IsActive}active{/if}">
										<a href="/{$productLink}/{$category->Handle}/">
											<span>{$category->Title}</span>
										</a>
									</li>
								{/foreach}
							</ul>
						{/if}
					{else}
						<p class="h4">{$contactdata.name}</p>

						{menu assign=sitemapMenu maxDepth=1}

						{include
							file='modules/widgets/menu/menu.tpl'
							items=$sitemapMenu
							classes='list-unstyled'
						}
					{/if}
				</div>
			</div>

			{if $access.user and $template.settings.SETTINGS_SHOW_MY_ACCOUNT && ($settings.shop_b2b_customer_approval || $settings.user_add)}
				<div class="col-s-4 col-m-6 col-l-3 col-xl-6">
					<div class="footer-account">
						<p class="h4">{$text.YOUR_ACCOUNT}</p>

						{menuStatic assign=userMenu}
						{include
							file='modules/widgets/menu/menu.tpl'
							items=$userMenu
							classes='list-unstyled'
						}
					</div>
				</div>
			{/if}

			<div class="col-s-4 col-m-6 col-l-3 col-xl-6">
				<div class="footer-contact">
					<p class="h4">{$text.CONTACT_TEXT}</p>

					<ul class="list-unstyled">
					{if $contactdata.name && $template.settings.SETTINGS_SHOW_CONTACT_TITLE}
						<li><span>{$contactdata.name}</span></li>
					{/if}
					{if $contactdata.address && $template.settings.SETTINGS_SHOW_CONTACT_ADDRESS}
						<li><span>{$contactdata.address}</span></li>
					{/if}
					{if ($contactdata.zipcode && $template.settings.SETTINGS_SHOW_CONTACT_ZIPCODE) || ($contactdata.city && $template.settings.SETTINGS_SHOW_CONTACT_CITY)}
						<li><span>
							{if $contactdata.zipcode && $template.settings.SETTINGS_SHOW_CONTACT_ZIPCODE}
								{$contactdata.zipcode}
							{/if}
							{if $contactdata.city && $template.settings.SETTINGS_SHOW_CONTACT_CITY}
								{$contactdata.city}
							{/if}
						</span></li>
					{/if}
					{if $contactdata.country && $template.settings.SETTINGS_SHOW_CONTACT_COUNTRY}
						<li><span>{$contactdata.country}</span></li>
					{/if}
					{if $contactdata.vatnumber && $template.settings.SETTINGS_SHOW_CONTACT_VAT_NUMBER}
						<li><span>{$text.VAT_NR}: {$contactdata.vatnumber}</span></li>
					{/if}
					{if $contactdata.bankinfo && $template.settings.SETTINGS_SHOW_CONTACT_BANK_NUMBER}
						<li><span>{$text.BANK_DETAILS}: {$contactdata.bankinfo}</span></li>
					{/if}
					</ul>

					<ul class="list-unstyled">
						{if $contactdata.phone && $template.settings.SETTINGS_SHOW_CONTACT_PHONE}
							<li><span>{$text.TELEPHONE}: {$contactdata.phone}</span></li>
						{/if}
						{if $contactdata.mobilephone && $template.settings.SETTINGS_SHOW_CONTACT_MOBILE}
							<li><span>{$text.MOBILE}: {$contactdata.mobilephone}</span></li>
						{/if}
						{if $contactdata.fax && $template.settings.SETTINGS_SHOW_CONTACT_FAX}
							<li><span>{$text.FAX}: {$contactdata.fax}</span></li>
						{/if}
						{if $contactdata.email && $template.settings.SETTINGS_SHOW_CONTACT_EMAIL}
							<li>
								<span class="contact-text">{$text.MAIL}</span><span class="contact-colon">:</span>
								{if $settings.spam_email_block}
									{$email = $contactdata.email|replace:'@':'---'}
									{placeholdImage assign=placeholder text=$email color=$template.settings.FONT_COLOR_FOOTER atreplace='---' transparent=true size=10}
									<a href="/obfuscated/" onclick="var m = '{$email}'; this.href = 'mailto:' + m.replace('---', '@'); return true;">
										<span>
											<img style="margin:0; vertical-align: middle;" alt="" src="{$placeholder->getRelativeFile()}">
										</span>
									</a>
								{else}
									<a href="mailto:{$contactdata.email}">{$contactdata.email}</a>
								{/if}
							</li>
						{/if}
					</ul>
					{if $template.settings.SETTINGS_SHOW_SITEMAP}
						<a href="/{$text.SITEMAP_LINK}/">{$text.SITEMAP_HEADLINE}</a>
					{/if}
				</div>
			</div>
			<div class="col-s-4 col-m-6 col-l-3 col-xl-6 is-hidden-print">
				<div class="footer-social">
					{if $access.user && $settings.news_signup}
						<p class="h4">{$text.NEWLSLETTER_MENU}</p>

						<div class="trailing-trip">
							{include file='modules/widgets/newsletter/newsletter.tpl' lazyloadRecaptcha=true}
						</div>
					{/if}
					{if $access.social}
						{if $settings.social_plugin_likebox_pageurl and $settings.social_facebook}
							<i class="fa fa-facebook-square social-icon social-facebook"></i> <a href="{$settings.social_plugin_likebox_pageurl}" class="social-link social-facebook" title="{$text.SOCIAL_BOX_FACEBOOK}" target="_blank" rel="noopener">{$text.SOCIAL_BOX_FACEBOOK}</a>
						{/if}
						{if $settings.social_twitter_pageurl and $settings.social_twitter}
							<i class="fa fa-twitter-square social-icon social-twitter"></i> <a href="{$settings.social_twitter_pageurl}" class="social-link social-twitter" title="{$text.SOCIAL_BOX_TWITTER}" target="_blank" rel="noopener">{$text.SOCIAL_BOX_TWITTER}</a>
						{/if}
						{if $settings.social_youtube_pageurl and $settings.social_youtube}
							<i class="fa fa-youtube-square social-icon social-youtube"></i> <a href="{$settings.social_youtube_pageurl}" class="social-link social-youtube" title="{$text.SOCIAL_BOX_YOUTUBE}" target="_blank" rel="noopener">{$text.SOCIAL_BOX_YOUTUBE}</a>
						{/if}
						{if $settings.social_linkedin_pageurl and $settings.social_linkedin}
							<i class="fa fa-linkedin-square social-icon social-linkedin"></i> <a href="{$settings.social_linkedin_pageurl}" class="social-link social-linkedin"  title="{$text.SOCIAL_BOX_LINKEDIN}" target="_blank" rel="noopener">{$text.SOCIAL_BOX_LINKEDIN}</a>
						{/if}
						{if $settings.social_instagram_pageurl}
							<i class="fa fa-instagram social-icon social-instagram"></i> <a href="{$settings.social_instagram_pageurl}" class="social-link social-instagram" title="{$text.SOCIAL_BOX_INSTAGRAM}" target="_blank" rel="noopener">{$text.SOCIAL_BOX_INSTAGRAM}</a>
						{/if}
					{/if}
				</div>
			</div>
		</div>
		{if $general.isShop}
			<div class="row">
				<div class="col-s-4 col-m-12 col-l-12 col-xl-24 text-center">
					<hr>
					{if $general.isShop && $template.settings.SETTINGS_SHOW_CART_ICONS_FOOTER}
						<div class="text-center footer-paymenticons">
							{if $template.settings.SETTINGS_SHOW_BIG_CART_ICONS_FOOTER}
								{$imageWidth  = 54}
								{$imageHeight = 30}
								{paymentIcons assign=icons iconSize=large}
							{else}
								{$imageWidth  = 34}
								{$imageHeight = 24}
								{paymentIcons assign=icons}
							{/if}
							{if $icons->getActualSize() gt 0}
								{include file="modules/widgets/image/placeholder-aspect.tpl" 
									width=$imageWidth 
									height=$imageHeight
									selector=":not(.ielt9) .footer-paymenticons"}
								<ul class="payment-icons list-unstyled">
									{foreach $icons->getData() as $icon}
										<li class="payments-icon payments-icon-{$icon@index} is-inline-block placeholder-wrapper">
											<span class="placeholder"></span>
											{img alt="{$icon->Title}" title="{$icon->Title}" src="{$template.cdn}{$icon->RelativeFile}"}
										</li>
									{/foreach}
								</ul>
							{/if}
						</div>
					{/if}
				</div>
			</div>
		{/if}
	</div>
</footer>
