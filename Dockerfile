FROM smartweb/nodejs-bower-grunt-bundle:latest

ADD . .

RUN npm ci \
    && npx grunt build

FROM scratch 

COPY --from=0 /data /data