module.exports = grunt => {

    //
    // Use node-sass as scss compiler
    const sass = require("sass");


    //
    // Define sources
    const scss_source = {
        "assets/css/template.closed.css": "source/scss/template.closed.scss",
        "assets/css/template.ie.css": "source/scss/template.ie.scss",
        "assets/css/template.css": "source/scss/template.scss"
    };


    //
    // Setup Grunt
    grunt.initConfig({


        //
        // watch for changes
        watch: {
            options: {
                atBegin: true
            },
            sass: {
                files: './source/scss/**/*.{scss,sass}',
                tasks: ['sass:watch']
            },
            js: {
                files: './source/**/*.js',
                tasks: 'uglify:watch'
            }
        },


        //
        // sass compilation with node-sass
        sass: {
            "options": {
                implementation: sass,
                sourceMap: false,
                includePaths: [
                    "./node_modules/breakpoint-sass/stylesheets/",
                    "./node_modules/breakpoint-slicer/stylesheets/",
                    "./node_modules/compass-mixins-template/lib/",
                    "./source/scss/"
                ]
            },
            "watch": {
                options: {
                    outputStyle: "expanded",
                    sourceComments: true
                },
                files: scss_source
            },
            "build": {
                options: {
                    outputStyle: "expanded",
                    sourceComments: false
                },
                files: scss_source
            }
        },

        //
        // css minification and concatination
        cssmin: {	
            combine: {	
                options: {	
                    keepSpecialComments: 0	
                },	
                files: {
                    "assets/css/template.closed.css": "assets/css/template.closed.css",
                    "assets/css/template.ie.css": "assets/css/template.ie.css",
                    "assets/css/template.css": "assets/css/template.css"
                }
            }
        },

        //
        // js minification
        uglify: {
            watch: {
                files: {
                    'assets/js/template.js': ['./source/libs/**/*.js', './source/js/**/*.js']
                },
                options: {
                    compress: false,
                    mangle: false,
                    beautify: true
                }
            },
            build: {
                files: {
                    'assets/js/template.js': ['./source/libs/**/*.js', './source/js/**/*.js']
                },
                options: {
                    sourceMap: true,
                    preserveComments: false,
                    mangle: true,
                    compress: {
                        drop_console: true
                    }
                }
            }
        }

    });


    //
    // Register tasks
    grunt.registerTask('default', ['watch']);
    grunt.registerTask('libs', ['uglify:build']);
    grunt.registerTask('build', ['sass:build', 'cssmin', 'libs']);


    //
    // Use grunt-tasks to load modules instead of
    require("load-grunt-tasks")(grunt, { scope: "devDependencies" });
};
